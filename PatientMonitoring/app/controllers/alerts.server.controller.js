﻿ const mongoose = require('mongoose');
 const Alert = mongoose.model('Alert');
 const User = mongoose.model('User');

//
function getErrorMessage(err) {
    if (err.errors) {
        for (let errName in err.errors) {
            if (err.errors[errName].message) return err.errors[errName].
                message;
        }
    } else {
        return 'Unknown server error';
    }
};
//
exports.create = function (req, res) {
    const alert = new Alert(req.body);
    alert.creator = req.user;
    alert.save((err) => {
        if (err) {
            return res.status(400).send({
                message: getErrorMessage(err)
            });
        } else {
            res.status(200).json(alert);
        }
    });
};
//
exports.list = function (req, res) {
    Alert.find().sort('-created').populate('creator', 'firstName lastName fullName').exec((err, alerts) => {
if (err) {
        return res.status(400).send({
            message: getErrorMessage(err)
        });
    } else {
        res.status(200).json(alerts);
    }
});
};
//
exports.alertsById = function (req, res, next, id) {
    Alert.findById(id)
        .populate('creator')
        .exec((err, alert) => {
            if (err) return next(err);
if (!alert) return next(new Error('Failed to load alert '
        + id));
    req.alert = alert;
    next();
});
};
//
exports.read = function (req, res) {
    res.status(200).json(req.alert);
};
//
exports.update = function (req, res) {
    const alert = req.alert;
    alert.title = req.body.title;
    alert.content = req.body.content;
    alert.save((err) => {
        if (err) {
            return res.status(400).send({
                message: getErrorMessage(err)
            });
        } else {
            res.status(200).json(alert);
        }
    });
};
//
exports.delete = function (req, res) {
    const alert = req.alert;
    alert.remove((err) => {
        if (err) {
            return res.status(400).send({
                message: getErrorMessage(err)
            });
        } else {
            res.status(200).json(alert);
        }
    });
};
//
//The hasAuthorization() middleware uses the req.alert and req.user objects
//to verify that the current user is the creator of the current alert
exports.hasAuthorization = function (req, res, next) {
    if (req.alert.creator.id !== req.user.id) {
        return res.status(403).send({
            message: 'User is not authorized'
        });
    }
    next();
};


