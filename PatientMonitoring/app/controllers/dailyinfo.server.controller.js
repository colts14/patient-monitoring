﻿const mongoose = require('mongoose');
const DailyInfo = mongoose.model('DailyInfo');
const User = mongoose.model('User');
//
function getErrorMessage(err) {
    if (err.errors) {
        for (let errName in err.errors) {
            if (err.errors[errName].message) return err.errors[errName].
                message;
        }
    } else {
        return 'Unknown server error';
    }
};
//
exports.create = function (req, res) {
    const dailyInfo = new DailyInfo(req.body);
    dailyInfo.creator = req.user;
    dailyInfo.save((err) => {
        if (err) {
            return res.status(400).send({
                message: getErrorMessage(err)
            });
        } else {
            res.status(200).json(dailyInfo);
        }
    });
};
//
exports.list = function (req, res) {
    DailyInfo.find().sort('-created')
        .populate('creator', 'firstName lastName fullName')
        //.populate('nurse', 'firstName lastName fullName')
        .exec((err, dailyInfo) => {
            if (err) {
                return res.status(400).send({
                    message: getErrorMessage(err)
                });
            } else {
                res.status(200).json(dailyInfo);
            }
        });
};
//
exports.dailyInfoById = function (req, res, next, id) {

    DailyInfo.findById(id)
        .populate('creator nurse')
        .exec((err, dailyInfo) => {
            if (err) return next(err);
            if (!dailyInfo) return next(new Error('Failed to load daily info '
                + id));
            req.dailyInfo = dailyInfo;
            next();
        });
};
//
exports.read = function (req, res) {
    res.status(200).json(req.dailyInfo);
};
//
//The hasAuthorization() middleware uses the req.article and req.user objects
//to verify that the current user is the creator of the current article
exports.hasAuthorization = function (req, res, next) {
    if (req.dailyInfo.creator.id !== req.user.id) {
        return res.status(403).send({
            message: 'User is not authorized'
        });
    }
    next();
};


