﻿const mongoose = require('mongoose');
const User = mongoose.model('User');
const DailyTip = mongoose.model('DailyTip');
//
function getErrorMessage(err) {
    if (err.errors) {
        for (let errName in err.errors) {
            if (err.errors[errName].message) return err.errors[errName].
                message;
        }
    } else {
        return 'Unknown server error';
    }
};
//
exports.send = function (req, res) {
    const dailytip = new DailyTip(req.body);
    dailytip.creator = req.user;
    dailytip.save((err) => {
        if (err) {
            return res.status(400).send({
                message: getErrorMessage(err)
            });
        } else {
            res.status(200).json(dailytip);
        }
    });
};
//
exports.read = function (req, res) {
    res.status(200).json(req.dailytip);
};
//
exports.list = function (req, res) {
    DailyTip.find().sort('-created').populate('creator', 'firstName lastName fullName').exec((err, dailytip) => {
        if (err) {
            return res.status(400).send({
                message: getErrorMessage(err)
            });
        } else {
            console.log(dailytip);
            console.log(err);
            res.status(200).json(dailytip);
        }
    });
};
//
exports.tipsById = function (req, res, next, id) {
    DailyTip.findById(id)
        .populate('creator')
        .exec((err, dailytip) => {
            if (err) return next(err);
            if (!dailytip) return next(new Error('Failed to load dailytip '
                + id));
            req.dailytip = dailytip;
            next();
        });
};