﻿const mongoose = require('mongoose');
const Illness = mongoose.model('Illness');
const User = mongoose.model('User');
//
function getErrorMessage(err) {
    if (err.errors) {
        for (let errName in err.errors) {
            if (err.errors[errName].message) return err.errors[errName].
                message;
        }
    } else {
        return 'Unknown server error';
    }
};
//
//
exports.create = function (req, res) {
    const illness = new Illness(req.body);
    illness.creator = req.user;
    illness.save((err) => {
        if (err) {
            return res.status(400).send({
                message: getErrorMessage(err)
            });
        } else {
            res.status(200).json(illness);
        }
    });
};
//
exports.read = function (req, res) {
    res.status(200).json(req.illness);
};
//
exports.list = function (req, res) {
    Illness.find()
        .exec((err, illness) => {
        if (err) {
            return res.status(400).send({
                message: getErrorMessage(err)
            });
        } else {
            res.status(200).json(illness);
        }
    });
};
//
exports.illById = function (req, res, next, id) {
    Illness.findById(id)
        .exec((err, illness) => {
            if (err) return next(err);
            if (!illness) return next(new Error('Failed to load illness '
                + id));
            req.illness = illness;
            next();
        });
};