﻿const mongoose = require('mongoose');
const VitalSigns = mongoose.model('VitalSigns');
const User = mongoose.model('User');
//
function getErrorMessage(err) {
    if (err.errors) {
        for (let errName in err.errors) {
            if (err.errors[errName].message) return err.errors[errName].
                message;
        }
    } else {
        return 'Unknown server error';
    }
};
//
exports.create = function (req, res) {
    const article = new Article(req.body);
    article.creator = req.user;
    article.save((err) => {
        if (err) {
            return res.status(400).send({
                message: getErrorMessage(err)
            });
        } else {
            res.status(200).json(article);
        }
    });
};
//
exports.record = function (req, res) {
    const vitalsigns = new VitalSigns(req.body);
    vitalsigns.creator = req.user;
    vitalsigns.save((err) => {
        if (err) {
            return res.status(400).send({
                message: getErrorMessage(err)
            });
        } else {
            res.status(200).json(vitalsigns);
        }
    });
};
//

exports.listrecords = function (req, res) {
    VitalSigns.find().sort('-created')
        .populate('creator', 'firstName lastName fullName')
        .populate('patient', 'firstName lastName fullName')
        .exec((err, vitalsigns) => {
if (err) {
        return res.status(400).send({
            message: getErrorMessage(err)
        });
    } else {
        res.status(200).json(vitalsigns);
    }
});
};
//
exports.vitalsignsById = function (req, res, next, id) {

    VitalSigns.findById(id)
        .populate('creator patient')
        .exec((err, vitalsigns) => {
        if (err) return next(err);
        if (!vitalsigns) return next(new Error('Failed to load vital signs '
            + id));
        req.vitalsigns = vitalsigns;
        next();
    });
};
//
exports.read = function (req, res) {
    res.status(200).json(req.vitalsigns);
};
//
exports.update = function (req, res) {
    const vitalsigns = req.vitalsigns;
    vitalsigns.bodytemp = req.body.bodytemp;
    vitalsigns.heartrate = req.body.heartrate;
    vitalsigns.bloodpressure = req.body.bloodpressure;
    vitalsigns.respiratoryrate = req.body.respiratoryrate;
    vitalsigns.save((err) => {
        if (err) {
            return res.status(400).send({
                message: getErrorMessage(err)
            });
        } else {
            res.status(200).json(vitalsigns);
        }
    });
};
//
exports.delete = function (req, res) {
    const vitalsigns = req.vitalsigns;
    vitalsigns.remove((err) => {
        if (err) {
            return res.status(400).send({
                message: getErrorMessage(err)
            });
        } else {
            res.status(200).json(vitalsigns);
        }
    });
};
//The hasAuthorization() middleware uses the req.article and req.user objects
//to verify that the current user is the creator of the current article
exports.hasAuthorization = function (req, res, next) {
    if (req.vitalsigns.creator.id !== req.user.id) {
        return res.status(403).send({
            message: 'User is not authorized'
        });
    }
    next();
};


