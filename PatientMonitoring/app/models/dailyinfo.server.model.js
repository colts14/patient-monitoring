﻿const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const DailyInfoSchema = new Schema({
    created: {
        type: Date,
        default: Date.now
    },
    content: {
        type: String,
        default: '',
        trim: true
    },
    nurse: {
        type: Schema.ObjectId,
        ref: 'User'
    },
    creator: {
        type: Schema.ObjectId,
        ref: 'User'
    }
});
mongoose.model('DailyInfo', DailyInfoSchema);