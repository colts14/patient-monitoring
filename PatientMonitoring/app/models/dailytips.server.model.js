﻿const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const DailyTipSchema = new Schema({
    sent: {
        type: Date,
        default: Date.now
    },
    tip: {
        type: String,
        required: 'Daily motivational tip is required'
    },
    patient: {
        type: Schema.ObjectId,
        ref: 'User'
    },
    creator: {
        type: Schema.ObjectId,
        ref: 'User'
    }
});
mongoose.model('DailyTip', DailyTipSchema);