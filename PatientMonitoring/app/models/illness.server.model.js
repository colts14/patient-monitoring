﻿const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const IllnessSchema = new Schema({
    condition: {
        type: String,
    },
    symptoms: {
        type: String,
    },
    advice: {
        type: String,
    }
});
mongoose.model('Illness', IllnessSchema);