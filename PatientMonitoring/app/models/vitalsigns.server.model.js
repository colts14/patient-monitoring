﻿const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const VitalSignsSchema = new Schema({
    created: {
        type: Date,
        default: Date.now
    },
    bodytemp: {
        type: String,
        default: '',
        trim: true
    },
    heartrate: {
        type: String,
        default: '',
        trim: true
    },
    bloodpressure: {
        type: String,
        default: '',
        trim: true
    },
    respiratoryrate: {
        type: String,
        default: '',
        trim: true
    },
    patient: {
        type: Schema.ObjectId,
        ref: 'User'
    },
    creator: {
        type: Schema.ObjectId,
        ref: 'User'
    }
});
mongoose.model('VitalSigns', VitalSignsSchema);