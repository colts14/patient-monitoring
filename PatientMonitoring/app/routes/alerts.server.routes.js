﻿const users = require('../../app/controllers/users.server.controller');
const alerts = require('../../app/controllers/alerts.server.controller');
//
module.exports = function (app) {
        app.route('/api/alerts/list')
            .get(alerts.list)
        app.route('/api/alerts/create')
            .get(users.listpatients)
            .post(users.requiresLogin, alerts.create);
        app.route('/api/alerts/list/:alertsId')
            .get(alerts.read)
            //.put(users.requiresLogin, alerts.hasAuthorization, alerts.
            //    update)
            //.delete(users.requiresLogin, alerts.hasAuthorization, alerts.
            //    delete);
        app.param('alertsId', alerts.alertsById);
};
