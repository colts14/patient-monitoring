﻿const users = require('../../app/controllers/users.server.controller');
const dailyInfo = require('../../app/controllers/dailyinfo.server.controller');
//
module.exports = function (app) {
    app.route('/api/info/list')
        .get(dailyInfo.list);
    app.route('/api/info/create')
        .get(users.listnurses)
        .post(users.requiresLogin, dailyInfo.create);
    app.route('/api/info/list/:dailyInfoId')
        .get(dailyInfo.read)
    app.param('dailyInfoId', dailyInfo.dailyInfoById);
};
