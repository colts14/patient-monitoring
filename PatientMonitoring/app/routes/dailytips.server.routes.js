﻿const users = require('../../app/controllers/users.server.controller');
const dailytips = require('../../app/controllers/dailytips.server.controller');
//
module.exports = function (app) {
    app.route('/api/tips/list')
        .get(dailytips.list);
    app.route('/api/tips/create')
        .get(users.listpatients)
        .post(users.requiresLogin, dailytips.send);
    app.route('/api/tips/:tipsId')
        .get(dailytips.read);
    app.param('tipsId', dailytips.tipsById);
};