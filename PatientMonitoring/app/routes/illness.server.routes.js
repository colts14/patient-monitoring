﻿const users = require('../../app/controllers/users.server.controller');
const illness = require('../../app/controllers/illness.server.controller');
//
module.exports = function (app) {
    app.route('/api/illness/list')
        .get(illness.list);
    app.route('/api/illness/create')
        .post(illness.create);
    app.route('/api/illness/list/:illId')
        .get(illness.read);
    app.param('illId', illness.illById);
};