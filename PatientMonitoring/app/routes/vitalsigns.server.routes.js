﻿const users = require('../../app/controllers/users.server.controller');
const vitals = require('../../app/controllers/vitalsigns.server.controller');

//
module.exports = function (app) {
        app.route('/api/vitals/record/list')
            .get(vitals.listrecords);
        app.route('/api/vitals/record')
            .get(users.listpatients)
            .post(users.requiresLogin, vitals.record);
        app.route('/api/vitals/record/:vitalsignsId')
            .get(vitals.read)
            .put(users.requiresLogin, vitals.hasAuthorization, vitals.
                update)
            .delete(users.requiresLogin, vitals.hasAuthorization, vitals.
                delete);
        app.param('vitalsignsId', vitals.vitalsignsById);
};
