﻿import { Component } from '@angular/core';
import { AlertsService } from './alerts.service';
@Component({
    selector: 'alerts',
    template: '<router-outlet></router-outlet>',
    providers: [AlertsService]
})
export class AlertsComponent { }
