﻿import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { AlertsRoutes } from './alerts.routes';
import { AlertsComponent } from './alerts.component';
import { CreateComponent } from './create/create.component';
import { ViewComponent } from './view/view.component';
import { ListComponent } from './list/list.component';

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        RouterModule.forChild(AlertsRoutes),
    ],
    declarations: [
        AlertsComponent,
        CreateComponent,
        ViewComponent,
        ListComponent,
    ]
})
export class AlertsModule { }
