System.register(["./alerts.component", "./create/create.component", "./view/view.component", "./list/list.component"], function (exports_1, context_1) {
    "use strict";
    var __moduleName = context_1 && context_1.id;
    var alerts_component_1, create_component_1, view_component_1, list_component_1, AlertsRoutes;
    return {
        setters: [
            function (alerts_component_1_1) {
                alerts_component_1 = alerts_component_1_1;
            },
            function (create_component_1_1) {
                create_component_1 = create_component_1_1;
            },
            function (view_component_1_1) {
                view_component_1 = view_component_1_1;
            },
            function (list_component_1_1) {
                list_component_1 = list_component_1_1;
            }
        ],
        execute: function () {
            exports_1("AlertsRoutes", AlertsRoutes = [{
                    path: 'alerts',
                    component: alerts_component_1.AlertsComponent,
                    children: [
                        //need default '' here
                        { path: 'list', component: list_component_1.ListComponent },
                        { path: 'create', component: create_component_1.CreateComponent },
                        { path: 'list/:alertsId', component: view_component_1.ViewComponent },
                    ],
                }]);
        }
    };
});
//# sourceMappingURL=alerts.routes.js.map