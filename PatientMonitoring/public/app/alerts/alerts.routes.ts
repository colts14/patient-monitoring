﻿import { Routes } from '@angular/router';
import { AlertsComponent } from './alerts.component';
import { CreateComponent } from './create/create.component';
import { ViewComponent } from './view/view.component';
import { ListComponent } from './list/list.component';

export const AlertsRoutes: Routes = [{
    path: 'alerts',
    component: AlertsComponent,
    children: [
    //need default '' here
        { path: 'list', component: ListComponent },
        { path: 'create', component: CreateComponent },
        { path: 'list/:alertsId', component: ViewComponent },
    ],
}];