﻿import 'rxjs/Rx';
import {Observable} from 'rxjs/Observable';
import {Injectable} from '@angular/core';
import {Http, Headers, Request, RequestMethod, Response} from '@angular/http';
@Injectable()
export class AlertsService {
    private _baseURL = 'api/alerts';
    constructor(private _http: Http) { }
    create(alert: any): Observable<any> {
        return this._http
            .post(this._baseURL, alert)
            .map((res: Response) => res.json())
            .catch(this.handleError);
    }
    //
    record(alerts: any): Observable<any> {
        return this._http
            .post(`${this._baseURL}/create`, alerts)
            .map((res: Response) => res.json())
            .catch(this.handleError);
    }
    //
    read(alertsId: string): Observable<any> {
        return this._http
            .get(`${this._baseURL}/list/${alertsId}`)
            .map((res: Response) => res.json())
            .catch(this.handleError);
    }
    //
    list(): Observable<any> {
        return this._http
            .get(`${this._baseURL}/list`)
            .map((res: Response) => res.json())
            .catch(this.handleError);
    }
    //
    getData(): Observable<any> {
        return this._http
            .get(`${this._baseURL}/create`)
            .map((res: Response) => res.json())
            .catch(this.handleError);
    }
 
    //
    private handleError(error: Response) {
        return Observable.throw(error.json().message || 'Server error');
    }
}
