﻿import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { AlertsService } from '../alerts.service';
 
@Component({
    selector: 'record',
    templateUrl: 'app/alerts/create/create.template.html'

})
export class CreateComponent {
    //patients: any;
    alerts: any = {};
    errorMessage: string;
    constructor(private _router: Router, private _alertsService: AlertsService) { }
    //ngOnInit() {
    //    this._alertsService.getData().subscribe(patients => this.patients
    //        = patients);
    //}
    record() {
        this._alertsService
            .record(this.alerts)
            .subscribe(recordedAlerts => this._router.navigate(['/alerts/create',
                recordedAlerts._id]),
            error => this.errorMessage = error);
    }
}

