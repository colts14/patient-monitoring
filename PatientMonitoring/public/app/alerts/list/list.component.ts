﻿import { Component } from '@angular/core';
import { AlertsService } from '../alerts.service';
@Component({
    selector: 'list',
    templateUrl: 'app/alerts/list/list.template.html'
})
export class ListComponent {
    alerts: any;
    errorMessage: string;
    constructor(private _alertsService: AlertsService) { }
    ngOnInit() {
        this._alertsService.list().subscribe(alerts => this.alerts
            = alerts);
    }
}

