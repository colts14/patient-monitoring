﻿import { Component } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { AuthenticationService } from '../../authentication/authentication.service';
import { AlertsService } from '../alerts.service';
@Component({
    selector: 'view',
    templateUrl: 'app/alerts/view/view.template.html',
})
export class ViewComponent {
    //user: any;
    alerts: any;
    paramsObserver: any;
    errorMessage: string;
    //
    constructor(private _router: Router,
        private _route: ActivatedRoute,
        private _authenticationService: AuthenticationService,
        private _alertsService: AlertsService) { }
    //
    ngOnInit() {
        //this.user = this._authenticationService.user
        this.paramsObserver = this._route.params.subscribe(params => {
            let alertsId = params['alertsId'];
            this._alertsService
                .read(alertsId)
                .subscribe(
                alerts => {
                    this.alerts = alerts;
                },
                error => this._router.navigate(['/alerts/list'])
                );
        });
    }
    //
    ngOnDestroy() {
        this.paramsObserver.unsubscribe();
    }
}
