﻿import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RouterModule } from '@angular/router';
import { HttpModule } from '@angular/http';
import { AppComponent } from './app.component';
import { AppRoutes } from './app.routes';
import { HomeModule } from './home/home.module';
import { AuthenticationService } from './authentication/authentication.service';
import { AuthenticationModule } from './authentication/authentication.module';
import { AlertsModule } from './alerts/alerts.module';
import { VitalSignsModule } from './vitalsigns/vitalsigns.module';
import { TipsModule } from './tips/tips.module';
import { InfoModule } from './info/info.module';
import { IllnessModule } from './illness/illness.module';

@NgModule({
    imports: [
        BrowserModule,
        HttpModule,
        AuthenticationModule,
        HomeModule,
        AlertsModule,
        VitalSignsModule,
        TipsModule,
        InfoModule,
        IllnessModule,
        RouterModule.forRoot(AppRoutes),
    ],
    declarations: [
        AppComponent,
    ],
    providers: [
        AuthenticationService
    ],
    bootstrap: [AppComponent]
})
export class AppModule { }

