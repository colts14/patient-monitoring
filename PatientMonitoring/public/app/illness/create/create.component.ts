﻿import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { IllnessService } from '../illness.service';

@Component({
    selector: 'create',
    templateUrl: 'app/illness/create/create.template.html'

})
export class CreateComponent {
    illnesses: any = {};
    errorMessage: string;
    constructor(private _router: Router, private _illnessService: IllnessService) { }
    create() {
        this._illnessService
            .create(this.illnesses)
            .subscribe(recordedIllness => this._router.navigate(['/']),
            error => this.errorMessage = error);
    }
}

