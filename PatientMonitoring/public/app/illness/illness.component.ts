﻿import { Component } from '@angular/core';
import { IllnessService } from './illness.service';
@Component({
    selector: 'ill',
    template: '<router-outlet></router-outlet>',
    providers: [IllnessService]
})
export class IllnessComponent { }
