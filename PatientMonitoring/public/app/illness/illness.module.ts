﻿import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { IllnessRoutes } from './illness.routes';
import { IllnessComponent } from './illness.component';
import { ViewComponent } from './view/view.component';
import { ListComponent } from './list/list.component';
import { CreateComponent } from './create/create.component';

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        RouterModule.forChild(IllnessRoutes),
    ],
    declarations: [
        IllnessComponent,
        ViewComponent,
        ListComponent,
        CreateComponent,
    ]
})
export class IllnessModule { }
