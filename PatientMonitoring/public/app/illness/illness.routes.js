System.register(["./illness.component", "./view/view.component", "./list/list.component", "./create/create.component"], function (exports_1, context_1) {
    "use strict";
    var __moduleName = context_1 && context_1.id;
    var illness_component_1, view_component_1, list_component_1, create_component_1, IllnessRoutes;
    return {
        setters: [
            function (illness_component_1_1) {
                illness_component_1 = illness_component_1_1;
            },
            function (view_component_1_1) {
                view_component_1 = view_component_1_1;
            },
            function (list_component_1_1) {
                list_component_1 = list_component_1_1;
            },
            function (create_component_1_1) {
                create_component_1 = create_component_1_1;
            }
        ],
        execute: function () {
            exports_1("IllnessRoutes", IllnessRoutes = [{
                    path: 'ill',
                    component: illness_component_1.IllnessComponent,
                    children: [
                        //need default '' here
                        { path: 'create', component: create_component_1.CreateComponent },
                        { path: 'list', component: list_component_1.ListComponent },
                        { path: 'list/:illId', component: view_component_1.ViewComponent },
                    ],
                }]);
        }
    };
});
//# sourceMappingURL=illness.routes.js.map