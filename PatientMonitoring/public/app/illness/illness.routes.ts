﻿import { Routes } from '@angular/router';
import { IllnessComponent } from './illness.component';
import { ViewComponent } from './view/view.component';
import { ListComponent } from './list/list.component';
import { CreateComponent } from './create/create.component';

export const IllnessRoutes: Routes = [{
    path: 'ill',
    component: IllnessComponent,
    children: [
        //need default '' here
        { path: 'create', component: CreateComponent },
        { path: 'list', component: ListComponent },
        { path: 'list/:illId', component: ViewComponent },
    ],
}];