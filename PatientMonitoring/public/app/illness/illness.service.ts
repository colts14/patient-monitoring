﻿import 'rxjs/Rx';
import {Observable} from 'rxjs/Observable';
import {Injectable} from '@angular/core';
import {Http, Headers, Request, RequestMethod, Response} from '@angular/http';
@Injectable()
export class IllnessService {
    private _baseURL = 'api/illness';
    constructor(private _http: Http) { }
    //
    create(illness: any): Observable<any> {
        return this._http
            .post(`${this._baseURL}/create`, illness)
            .map((res: Response) => res.json())
            .catch(this.handleError);
    }
    //
    read(illId: string): Observable<any> {
        return this._http
            .get(`${this._baseURL}/list/${illId}`)
            .map((res: Response) => res.json())
            .catch(this.handleError);
    }
    //
    list(): Observable<any> {
        return this._http
            .get(`${this._baseURL}/list`)
            .map((res: Response) => res.json())
            .catch(this.handleError);
    }
    //
    private handleError(error: Response) {
        return Observable.throw(error.json().message || 'Server error');
    }
}
