﻿import { Component } from '@angular/core';
import { IllnessService } from '../illness.service';

@Component({
    selector: 'list',
    templateUrl: 'app/illness/list/list.template.html'
})
export class ListComponent {
    illnesses: any;
    errorMessage: string;
    constructor(private _illnessService: IllnessService) { }
    ngOnInit() {
        this._illnessService.list().subscribe(illnesses => this.illnesses
            = illnesses);
    }
}