﻿import { Component } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { IllnessService } from '../illness.service';

@Component({
    selector: 'view',
    templateUrl: 'app/illness/view/view.template.html',
})
export class ViewComponent {
    illness: any;
    paramsObserver: any;
    errorMessage: string;
    //
    constructor(private _router: Router,
        private _route: ActivatedRoute,
        private _illnessService: IllnessService) { }
    //
    ngOnInit() {
        this.paramsObserver = this._route.params.subscribe(params => {
            let illId = params['illId'];
            this._illnessService
                .read(illId)
                .subscribe(
                illness => {
                    this.illness = illness;
                },
                error => this._router.navigate(['/illness/list'])
                );
        });
    }
    //
    ngOnDestroy() {
        this.paramsObserver.unsubscribe();
    }
}
