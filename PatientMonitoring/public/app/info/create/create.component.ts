﻿import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { InfoService } from '../info.service';

@Component({
    selector: 'create',
    templateUrl: 'app/info/create/create.template.html'
})
export class CreateComponent {
    nurses: any;
    dailyinfo: any = {};
    errorMessage: string;
    constructor(private _router: Router,
        private _dailyInfoService: InfoService) { }
    ngOnInit() {
        this._dailyInfoService.listNurse().subscribe(nurses => this.nurses
            = nurses);
    };
    create() {
        this._dailyInfoService
            .create(this.dailyinfo)
            .subscribe(createdInfo => this._router.navigate(['/list/create']),
            error => this.errorMessage = error);
    }
}
