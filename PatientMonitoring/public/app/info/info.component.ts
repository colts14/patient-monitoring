﻿import { Component } from '@angular/core';
import { InfoService } from './info.service';
@Component({
    selector: 'info',
    template: '<router-outlet></router-outlet>',
    providers: [InfoService]
})
export class InfoComponent { }
