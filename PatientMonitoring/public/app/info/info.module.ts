﻿import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { InfoRoutes } from './info.routes';
import { InfoComponent } from './info.component';
import { CreateComponent } from './create/create.component';
import { ViewComponent } from './view/view.component';
import { ListComponent } from './list/list.component';

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        RouterModule.forChild(InfoRoutes),
    ],
    declarations: [
        InfoComponent,
        CreateComponent,
        ViewComponent,
        ListComponent,
    ]
})
export class InfoModule { }
