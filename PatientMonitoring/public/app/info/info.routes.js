System.register(["./info.component", "./view/view.component", "./list/list.component", "./create/create.component"], function (exports_1, context_1) {
    "use strict";
    var __moduleName = context_1 && context_1.id;
    var info_component_1, view_component_1, list_component_1, create_component_1, InfoRoutes;
    return {
        setters: [
            function (info_component_1_1) {
                info_component_1 = info_component_1_1;
            },
            function (view_component_1_1) {
                view_component_1 = view_component_1_1;
            },
            function (list_component_1_1) {
                list_component_1 = list_component_1_1;
            },
            function (create_component_1_1) {
                create_component_1 = create_component_1_1;
            }
        ],
        execute: function () {
            exports_1("InfoRoutes", InfoRoutes = [{
                    path: 'info',
                    component: info_component_1.InfoComponent,
                    children: [
                        //need default '' here
                        { path: 'list', component: list_component_1.ListComponent },
                        { path: 'create', component: create_component_1.CreateComponent },
                        { path: 'list/:dailyInfoId', component: view_component_1.ViewComponent },
                    ],
                }]);
        }
    };
});
//# sourceMappingURL=info.routes.js.map