﻿import { Routes } from '@angular/router';
import { InfoComponent } from './info.component';
import { ViewComponent } from './view/view.component';
import { ListComponent } from './list/list.component';
import { CreateComponent } from './create/create.component';

export const InfoRoutes: Routes = [{
    path: 'info',
    component: InfoComponent,
    children: [
    //need default '' here
        { path: 'list', component: ListComponent },
        { path: 'create', component: CreateComponent },
        { path: 'list/:dailyInfoId', component: ViewComponent },
    ],
}];