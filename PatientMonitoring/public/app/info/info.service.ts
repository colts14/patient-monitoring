﻿import 'rxjs/Rx';
import {Observable} from 'rxjs/Observable';
import {Injectable} from '@angular/core';
import {Http, Headers, Request, RequestMethod, Response} from '@angular/http';
@Injectable()
export class InfoService {
    private _baseURL = 'api/info';
    constructor(private _http: Http) { }
    //
    create(dailyinfo: any): Observable<any> {
        return this._http
            .post(`${this._baseURL}/create`, dailyinfo)
            .map((res: Response) => res.json())
            .catch(this.handleError);
    }
    //
    read(dailyInfoId: string): Observable<any> {
        return this._http
            .get(`${this._baseURL}/list/${dailyInfoId}`)
            .map((res: Response) => res.json())
            .catch(this.handleError);
    }
    //
    list(): Observable<any> {
        return this._http
            .get(`${this._baseURL}/list`)
            .map((res: Response) => res.json())
            .catch(this.handleError);
    }
    //
    getData(): Observable<any> {
        return this._http
            .get(`${this._baseURL}/list`)
            .map((res: Response) => res.json())
            .catch(this.handleError);
    }
    //
    listNurse(): Observable<any> {
        return this._http
            .get(`${this._baseURL}/create`)
            .map((res: Response) => res.json())
            .catch(this.handleError);
    }
    //
    private handleError(error: Response) {
        return Observable.throw(error.json().message || 'Server error');
    }
}
