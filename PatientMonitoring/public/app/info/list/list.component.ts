﻿import { Component } from '@angular/core';
import { InfoService } from '../info.service';
import { AuthenticationService } from '../../authentication/authentication.service';

@Component({
    selector: 'list',
    templateUrl: 'app/info/list/list.template.html'
})
export class ListComponent {
    dailyinfo: any;
    user: any;
    paramsObserver: any;
    errorMessage: string;
    constructor(private _dailyInfoService: InfoService,
        private _authenticationService: AuthenticationService
    ) { }
    ngOnInit() {
        this.user = this._authenticationService.user
        this._dailyInfoService.list().subscribe(dailyinfo => this.dailyinfo
            = dailyinfo);
    }
}