﻿import { Component } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { AuthenticationService } from '../../authentication/authentication.service';
import { InfoService } from '../info.service';
@Component({
    selector: 'view',
    templateUrl: 'app/info/view/view.template.html',
})
export class ViewComponent {
    user: any;
    dailyinfo: any;
    paramsObserver: any;
    errorMessage: string;
    allowEdit: boolean = false;
    //
    constructor(private _router: Router,
        private _route: ActivatedRoute,
        private _authenticationService: AuthenticationService,
        private _dailyInfoService: InfoService) { }
    //
    ngOnInit() {
        this.user = this._authenticationService.user
        this.paramsObserver = this._route.params.subscribe(params => {
            let dailyInfoId = params['dailyInfoId'];
            this._dailyInfoService
                .read(dailyInfoId)
                .subscribe(
                dailyinfo => {
                    this.dailyinfo = dailyinfo;
                },
                error => this._router.navigate(['/vitals'])
                );
        });
    }
    //
    ngOnDestroy() {
        this.paramsObserver.unsubscribe();
    }
}
