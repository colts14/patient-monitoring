﻿import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { TipsService } from '../tips.service';
 
@Component({
    selector: 'create',
    templateUrl: 'app/tips/create/create.template.html'
})
export class CreateComponent {
    patients: any;
    dailytip: any = {};
    errorMessage: string;
    constructor(private _router: Router,
        private _tipsService: TipsService) { }
    ngOnInit() {
        this._tipsService.listPatients().subscribe(patients => this.patients
            = patients);
    }
    sendtip() {
        this._tipsService
            .sendTip(this.dailytip)
            .subscribe(recordedDailyTip => this._router.navigate(['/']),
            error => this.errorMessage = error);
    }
    
}