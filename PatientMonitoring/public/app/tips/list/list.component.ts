﻿import { Component } from '@angular/core';
import { AuthenticationService } from '../../authentication/authentication.service';
import { TipsService } from '../tips.service';
@Component({
    selector: 'list',
    templateUrl: 'app/tips/list/list.template.html'
})
export class ListComponent {
    tips: any;
    user: any;
    paramsObserver: any;
    errorMessage: string;
    constructor(private _tipsService: TipsService,
        private _authenticationService: AuthenticationService
    ) { }
    ngOnInit() {
        this.user = this._authenticationService.user
        this._tipsService.list().subscribe(tips => this.tips
            = tips);
    }
}