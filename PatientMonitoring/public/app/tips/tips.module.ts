﻿import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { TipsRoutes } from './tips.routes';
import { TipsComponent } from './tips.component';
import { ViewComponent } from './view/view.component';
import { CreateComponent } from './create/create.component';
import { ListComponent } from './list/list.component';
@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        RouterModule.forChild(TipsRoutes),
    ],
    declarations: [
        TipsComponent,
        ViewComponent,
        CreateComponent,
        ListComponent,
    ]
})
export class TipsModule { }
