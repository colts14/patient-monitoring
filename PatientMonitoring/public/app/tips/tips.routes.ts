﻿import { Routes } from '@angular/router';
import { TipsComponent } from './tips.component';
import { ViewComponent } from './view/view.component';
import { CreateComponent } from './create/create.component';
import { ListComponent } from './list/list.component';
export const TipsRoutes: Routes = [{
    path: 'tips',
    component: TipsComponent,
    children: [
        //need default '' here
        { path: 'create', component: CreateComponent },
        { path: 'list', component: ListComponent },
        { path: ':tipsId', component: ViewComponent },
    ],
}];