﻿import 'rxjs/Rx';
import {Observable} from 'rxjs/Observable';
import {Injectable} from '@angular/core';
import {Http, Headers, Request, RequestMethod, Response} from '@angular/http';
@Injectable()
export class TipsService {
    private _baseURL = 'api/tips';
    constructor(private _http: Http) { }
    //
    sendTip(dailytip: any): Observable<any> {
        return this._http
            .post(`${this._baseURL}/create`, dailytip)
            .map((res: Response) => res.json())
            .catch(this.handleError);
    }
    //
    list(): Observable<any> {
        return this._http
            .get(`${this._baseURL}/list`)
            .map((res: Response) => res.json())
            .catch(this.handleError);
    }
    //
    read(tipsId: any): Observable<any> {
        return this._http
            .get(`${this._baseURL}/${tipsId}`)
            .map((res: Response) => res.json())
            .catch(this.handleError);
    }
    //
    listPatients(): Observable<any> {
        return this._http
            .get(`${this._baseURL}/create`)
            .map((res: Response) => res.json())
            .catch(this.handleError);
    }
    //
    getData(): Observable<any> {
        return this._http
            .get(`${this._baseURL}/list`)
            .map((res: Response) => res.json())
            .catch(this.handleError);
    }
    //
    private handleError(error: Response) {
        return Observable.throw(error.json().message || 'Server error');
    }
}
