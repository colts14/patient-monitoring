﻿import { Component } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { AuthenticationService } from '../../authentication/authentication.service';
import { TipsService } from '../tips.service';
@Component({
    selector: 'view',
    templateUrl: 'app/tips/view/view.template.html',
})
export class ViewComponent {
    user: any;
    tips: any;
    paramsObserver: any;
    errorMessage: string;
    allowEdit: boolean = false;
    //
    constructor(private _router: Router,
        private _route: ActivatedRoute,
        private _authenticationService: AuthenticationService,
        private _tipsService: TipsService) { }
    //
    ngOnInit() {
        this.user = this._authenticationService.user
        this.paramsObserver = this._route.params.subscribe(params => {
            let tipsId = params['tipsId'];
            this._tipsService
                .read(tipsId)
                .subscribe(
                tips => {
                    this.tips = tips;
                },
                error => this._router.navigate(['/tips'])
                );
        });
    }
    //
    ngOnDestroy() {
        this.paramsObserver.unsubscribe();
    }
}
