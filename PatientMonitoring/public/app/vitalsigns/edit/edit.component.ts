﻿import { Component } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { VitalSignsService } from '../vitalsigns.service';
@Component({
    selector: 'edit',
    templateUrl: 'app/vitalsigns/edit/edit.template.html'
})
export class EditComponent {
    vitalsigns: any = {};
    errorMessage: string;
    paramsObserver: any;
    constructor(private _router: Router,
        private _route: ActivatedRoute,
        private _vitalSignsService: VitalSignsService) { }
    ngOnInit() {
        this.paramsObserver = this._route.params.subscribe(params => {
            let vitalsignsId = params['vitalsignsId'];
            this._vitalSignsService.read(vitalsignsId).subscribe(vitalsigns => {
                this.vitalsigns = vitalsigns;
            },
                error => this._router.navigate(['/vitals']));
});
    }
    ngOnDestroy() {
        this.paramsObserver.unsubscribe();
    }
    update() {
        this._vitalSignsService.update(this.vitalsigns).subscribe(savedArticle => this._router.navigate(['/vitals/record', savedArticle._id]),
            error => this.errorMessage =
                error);
    }
}