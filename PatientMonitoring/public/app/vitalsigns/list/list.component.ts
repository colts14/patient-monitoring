﻿import { Component } from '@angular/core';
import { VitalSignsService } from '../vitalsigns.service';
import { AuthenticationService } from '../../authentication/authentication.service';

@Component({
    selector: 'list',
    templateUrl: 'app/vitalsigns/list/list.template.html'
})
export class ListComponent {
    vitalsigns: any;
    user: any;
    paramsObserver: any;
    errorMessage: string;
    constructor(private _vitalSignsService: VitalSignsService,
        private _authenticationService: AuthenticationService) { } 
    ngOnInit() {
        this.user = this._authenticationService.user
        this._vitalSignsService.list().subscribe(vitalsigns => this.vitalsigns
            = vitalsigns);
    }
}