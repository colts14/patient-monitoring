﻿import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { VitalSignsService } from '../vitalsigns.service';
 
@Component({
    selector: 'record',
    templateUrl: 'app/vitalsigns/record/record.template.html'

})
export class RecordComponent {
    patients: any;
    vitalsigns: any = {};
    errorMessage: string;
    constructor(private _router: Router, private _vitalSignsService: VitalSignsService) { }
    ngOnInit() {
        this._vitalSignsService.getData().subscribe(patients => this.patients
            = patients);
    }
    record() {
        this._vitalSignsService
            .record(this.vitalsigns)
            .subscribe(recordedVitalSigns => this._router.navigate(['/vitals/record',
                recordedVitalSigns._id]),
            error => this.errorMessage = error);
    }
}

