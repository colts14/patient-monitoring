﻿import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { VitalSignsRoutes } from './vitalsigns.routes';
import { VitalSignsComponent } from './vitalsigns.component';
import { RecordComponent } from './record/record.component';
import { ViewComponent } from './view/view.component';
import { EditComponent } from './edit/edit.component';
import { ListComponent } from './list/list.component';

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        RouterModule.forChild(VitalSignsRoutes),
    ],
    declarations: [
        VitalSignsComponent,
        RecordComponent,
        ViewComponent,
        EditComponent,
        ListComponent,
    ]
})
export class VitalSignsModule { }
