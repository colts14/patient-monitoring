System.register(["./vitalsigns.component", "./record/record.component", "./view/view.component", "./edit/edit.component", "./list/list.component"], function (exports_1, context_1) {
    "use strict";
    var __moduleName = context_1 && context_1.id;
    var vitalsigns_component_1, record_component_1, view_component_1, edit_component_1, list_component_1, VitalSignsRoutes;
    return {
        setters: [
            function (vitalsigns_component_1_1) {
                vitalsigns_component_1 = vitalsigns_component_1_1;
            },
            function (record_component_1_1) {
                record_component_1 = record_component_1_1;
            },
            function (view_component_1_1) {
                view_component_1 = view_component_1_1;
            },
            function (edit_component_1_1) {
                edit_component_1 = edit_component_1_1;
            },
            function (list_component_1_1) {
                list_component_1 = list_component_1_1;
            }
        ],
        execute: function () {
            exports_1("VitalSignsRoutes", VitalSignsRoutes = [{
                    path: 'vitals',
                    component: vitalsigns_component_1.VitalSignsComponent,
                    children: [
                        //need default '' here
                        { path: 'record/list', component: list_component_1.ListComponent },
                        { path: 'record', component: record_component_1.RecordComponent },
                        { path: 'record/:vitalsignsId', component: view_component_1.ViewComponent },
                        { path: 'record/:vitalsignsId/edit', component: edit_component_1.EditComponent },
                    ],
                }]);
        }
    };
});
//# sourceMappingURL=vitalsigns.routes.js.map