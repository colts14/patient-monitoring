﻿import { Routes } from '@angular/router';
import { VitalSignsComponent } from './vitalsigns.component';
import { RecordComponent } from './record/record.component';
import { ViewComponent } from './view/view.component';
import { EditComponent } from './edit/edit.component';
import { ListComponent } from './list/list.component';

export const VitalSignsRoutes: Routes = [{
    path: 'vitals',
    component: VitalSignsComponent,
    children: [
    //need default '' here
        { path: 'record/list', component: ListComponent },
        { path: 'record', component: RecordComponent },
        { path: 'record/:vitalsignsId', component: ViewComponent },
        { path: 'record/:vitalsignsId/edit', component: EditComponent },
    ],
}];