﻿import 'rxjs/Rx';
import {Observable} from 'rxjs/Observable';
import {Injectable} from '@angular/core';
import {Http, Headers, Request, RequestMethod, Response} from '@angular/http';
@Injectable()
export class VitalSignsService {
    private _baseURL = 'api/vitals';
    constructor(private _http: Http) { }
    //
    record(vitalsigns: any): Observable<any> {
        return this._http
            .post(`${this._baseURL}/record`, vitalsigns)
            .map((res: Response) => res.json())
            .catch(this.handleError);
    }
    //
    read(vitalsignsId: string): Observable<any> {
        return this._http
            .get(`${this._baseURL}/record/${vitalsignsId}`)
            .map((res: Response) => res.json())
            .catch(this.handleError);
    }
    //
    update(vitalsigns: any): Observable<any> {
        return this._http
            .put(`${this._baseURL}/record/${vitalsigns._id}`, vitalsigns).map((res: Response) => res.json())
            .catch(this.handleError);
    }
    //
    delete(vitalsignsId: any): Observable<any> {
        return this._http
            .delete(`${this._baseURL}/record/${vitalsignsId}`)
            .map((res: Response) => res.json())
            .catch(this.handleError);
    }
    list(): Observable<any> {
        return this._http
            .get(`${this._baseURL}/record/list`)
            .map((res: Response) => res.json())
            .catch(this.handleError);
    }
    //
    getData(): Observable<any> {
        return this._http
            .get(`${this._baseURL}/record`)
            .map((res: Response) => res.json())
            .catch(this.handleError);
    }
    //
    listPatients(): Observable<any> {
        return this._http
            .get(`${this._baseURL}/tip`)
            .map((res: Response) => res.json())
            .catch(this.handleError);
    }
    //
    private handleError(error: Response) {
        return Observable.throw(error.json().message || 'Server error');
    }
}
